<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>両替</title>

<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
	<div id="app">
		<div class="row">
			<div class="col s12 l4 offset-l4">
				<h1>為替レート</h1>
				<p>為替レートが取得できる、便利なやつです</p>
				<h2>フォーム</h2>
				<div class="card-panel z-depth-3">
					<div class="row">
						<form action="/rate/home" method="get">
							<div class="input-field col s6">
								<select name="code" required v-model="code">
									<option value="" disabled>選んでね</option>
									<c:forEach var="rate" items="${ rates }">
										<option value="${ rate.key }"
											<c:if test="${ rate.key == pairCode }">selected</c:if>>${ rate.key.substring(0, 3)}/${ rate.key.substring(3)}</option>
									</c:forEach>
								</select> <label>選択</label>
							</div>
							<div class="col s6">
								<div class="input-field inline">
									<input id="last_name" type="number" min="1" max="10000"
										class="validate" name="money" value="${ money }" required>
									<label for="last_name">金額</label>
								</div>
								{{ from }}
							</div>
							<div class="col s12">
								<button class="btn waves-effect waves-light" type="submit">
									更新<i class="material-icons right">send</i>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<c:if test="${ result != null }">
			<div class="row">
				<div class="col s12 l6">
					<h2>レート</h2>
					<div class="card-panel z-depth-3">
						<div>
							<table>
								<tr>
									<td>最高値</td>
									<td>${ Math.round(result.high * money * 100) / 100 }{{to}}</td>
								</tr>
								<tr>
									<td>開始値</td>
									<td>${ Math.round(result.open * money * 100) / 100 }{{to}}</td>
								</tr>
								<tr>
									<td>売値</td>
									<td>${ Math.round(result.bid * money * 100) / 100 }{{to}}</td>
								</tr>
								<tr>
									<td>買値</td>
									<td>${ Math.round(result.ask * money * 100) / 100 }{{to}}</td>
								</tr>
								<tr>
									<td>最安値</td>
									<td>${ Math.round(result.low * money * 100) / 100 }{{to}}</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="col s12 l6">
					<h2>グラフ</h2>
					<div class="card-panel z-depth-3">
						<canvas id="chart" width="100%"></canvas>
					</div>
				</div>
			</div>
		</c:if>
	</div>
	<script type="text/javascript"
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
	<script>
		var app = new Vue({
			el : '#app',
			data : {
				code : '${ pairCode }',
				from : '',
				to : '',
				chart : null,
				data: [
					${result.high},
					${result.open},
					${result.bid},
					${result.ask},
					${result.low}
				],
				min: 0,
				max: 0,
			},
			watch : {
				code : function() {
					this.from = this.code.substring(0, 3)
				}
			},
			mounted : function() {
				this.min = Math.min.apply(null, this.data)
				this.max = Math.max.apply(null, this.data)
				let sub = (this.max - this.min) / 10
				this.min -= sub
				this.max += sub

				this.from = this.code.substring(0, 3)
				this.to = this.code.substring(3)
				const ctx = document.getElementById('chart')
				this.chart = new Chart(ctx, {
					/* グラフタイプ */
					type : 'bar',
					/* データ */
					data : {
						/* ラベル */
						labels : [ '最高値', '開始値', '売値', '買値', '最安値'],
						datasets : [ {
							/* 実際のデータ */
							data : this.data,
							/* グラフの色の設定 */
							backgroundColor : [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)'
							],
							borderColor : [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)'
							],
							borderWidth : 1
						} ]
					},
					options : {
						/* レスポンシブ対応の有効化 */
						responsive: true,
						title: {
							display: true,
							text: '${ pairCode.substring(0, 3) }' +  '→ ' + '${ pairCode.substring(3) }'
						},
						/* 判例は非表示 */
						legend: {
							display: false
			           },
						scales : {
							/* Y軸の設定 */
							yAxes : [ {
								scaleLabel: {
									display: true,
									labelString: 'レート'
								},
								/* 目盛りの設定 */
								ticks : {
									min: this.min,
									max: this.max
								}
							} ],
							/* X軸の設定 */
							xAxes : [{
								scaleLabel: {
									display: true,
									labelString: '項目'
								},
							}]
						}
					}

				})
			}
		});
		M.AutoInit();
	</script>
</body>
</html>