package model;

import java.util.Date;

public class Rate {
	/**
	 * 両替前
	 */
	private String from;

	/**
	 * 両替後
	 */
	private String to;

	/**
	 * タイムスタンプ
	 */
	private Date timestamp;

	/**
	 * 最高値
	 */
	private double high;

	/**
	 * 開始値
	 */
	private double open;

	/**
	 * 売値
	 */
	private double bid;

	/**
	 * 買値
	 */
	private double ask;

	/**
	 * 最安値
	 */
	private double low;

	/**
	 * Jsonを元にRateを作るコンストラクタ
	 * @param json
	 */
	public Rate(Json json) {
		Date timestamp = new Date();
		String from = json.getCurrencyPairCode().substring(0, 3);
		String to = json.getCurrencyPairCode().substring(3);
		this.timestamp = timestamp;
		this.from = from;
		this.to = to;
		this.high = json.getHigh();
		this.open = json.getOpen();
		this.bid = json.getBid();
		this.ask = json.getAsk();
		this.low = json.getLow();
	}

	public Rate(Date timestamp, String from, String to, double high, double open, double bid, double ask, double low) {
		this.timestamp = timestamp;
		this.from = from;
		this.to = to;
		this.high = high;
		this.open = open;
		this.bid = bid;
		this.ask = ask;
		this.low = low;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getBid() {
		return bid;
	}

	public void setBid(double bid) {
		this.bid = bid;
	}

	public double getAsk() {
		return ask;
	}

	public void setAsk(double ask) {
		this.ask = ask;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	/**
	 * @return from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from セットする from
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to セットする to
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp セットする timestamp
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
}
