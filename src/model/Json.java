package model;

public class Json {
	/**
	 * 最高値
	 */
	private double high;

	/**
	 * 開始値
	 */
	private double open;

	/**
	 * 売値
	 */
	private double bid;

	/**
	 * pairCode
	 */
	private String currencyPairCode;

	/**
	 * 買値
	 */
	private double ask;

	/**
	 * 最安値
	 */
	private double low;

	/**
	 * @return high
	 */
	public double getHigh() {
		return high;
	}

	/**
	 * @param high セットする high
	 */
	public void setHigh(double high) {
		this.high = high;
	}

	/**
	 * @return open
	 */
	public double getOpen() {
		return open;
	}

	/**
	 * @param open セットする open
	 */
	public void setOpen(double open) {
		this.open = open;
	}

	/**
	 * @return bid
	 */
	public double getBid() {
		return bid;
	}

	/**
	 * @param bid セットする bid
	 */
	public void setBid(double bid) {
		this.bid = bid;
	}

	/**
	 * @return currencyPairCode
	 */
	public String getCurrencyPairCode() {
		return currencyPairCode;
	}

	/**
	 * @param currencyPairCode セットする currencyPairCode
	 */
	public void setCurrencyPairCode(String currencyPairCode) {
		this.currencyPairCode = currencyPairCode;
	}

	/**
	 * @return ask
	 */
	public double getAsk() {
		return ask;
	}

	/**
	 * @param ask セットする ask
	 */
	public void setAsk(double ask) {
		this.ask = ask;
	}

	/**
	 * @return low
	 */
	public double getLow() {
		return low;
	}

	/**
	 * @param low セットする low
	 */
	public void setLow(double low) {
		this.low = low;
	}
}
