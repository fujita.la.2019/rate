package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import model.dao.DAOException;
import model.dao.RateDAO;

public class RateConnection {
	/**
	 * レート取得APIのURL
	 */
	private final String url = "https://www.gaitameonline.com/rateaj/getrate";

	/**
	 * コンストラクタ
	 */
	public RateConnection() {
		this.updateRate();
	}

	public void updateRate() {
		HttpURLConnection connection = null;
		InputStream stream = null;
		BufferedReader reader = null;

		try {
			//接続先URLの指定
			URL url = new URL(this.getUrl());

			//コネクションの確立
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();

			//HTTP ステータスコード取得
			int status = connection.getResponseCode();

			//200が返ってくれば、処理続行
			if(status == HttpURLConnection.HTTP_OK) {
				//読み込み準備
				stream = connection.getInputStream();
				reader = new BufferedReader(new InputStreamReader(stream));

				StringBuilder output = new StringBuilder();
				String line;

				while ((line = reader.readLine()) != null) {
					output.append(line);
				}

				this.format(output.toString());
			}
		}catch(IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if(reader != null) {
					reader.close();
				}
				if(connection != null) {
					connection.disconnect();
				}
			}catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 扱いやすいよう、受け取ったJSONから必要なデータをフォーマット
	 * @param json 対象のJSONテキスト
	 */
	private void format(String json) {
		Gson gson = new Gson();
		Type type = new TypeToken<List<Json>>(){}.getType();
		List<Rate> table = gson.fromJson(json, type);
		try {
			RateDAO dao = new RateDAO();
			dao.insert(table);
		} catch (DAOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @return url
	 */
	public String getUrl() {
		return url;
	}

}
