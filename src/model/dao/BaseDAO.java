package model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDAO {
	/**
	 * JDBCドライバ
	 */
	private final String DRIVER = "org.postgresql.Driver";

	/**
	 * URL
	 */
	private final String URL = "jdbc:postgresql:rate";

	/**
	 * ユーザー名
	 */
	private final String USER = "postgres";

	/**
	 * パスワード
	 */
	private final String PASSWORD = "himitu";

	/**
	 * DBとのコネクション
	 */
	private Connection con;

	/**
	 * コンストラクタ
	 * @throws DAOException
	 */
	public BaseDAO() throws DAOException {
		this.getConection();
	}

	/**
	 * DB接続
	 * @throws DAOException
	 */
	protected void getConection() throws DAOException {
		try {
			//DBに接続
			Class.forName(this.DRIVER);
			this.con = DriverManager.getConnection(this.URL, this.USER, this.PASSWORD);
		}catch(Exception e) {
			throw new DAOException("接続に失敗しました");
		}
	}

	/**
	 *
	 * @throws SQLException
	 */
	protected void close() throws SQLException {
		if(con != null) {
			con.close();
			con = null;
		}
	}

	/**
	 * @param con セットする con
	 */
	protected void setCon(Connection con) {
		this.con = con;
	}

	/**
	 * @return con
	 */
	protected Connection getCon() {
		return con;
	}
}
