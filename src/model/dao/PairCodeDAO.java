package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PairCodeDAO extends BaseDAO{
	/**
	 * コンストラクタ
	 * @throws DAOException
	 */
	public PairCodeDAO() throws DAOException {
		super();
	}

	/**
	 * すべてのペアコードを取得
	 * @return
	 * @throws DAOException
	 */
	public Map<String, Map<String, String>> getAllPair() throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT * FROM pair";
		try(PreparedStatement st = this.getCon().prepareStatement(SQL); ResultSet rs = st.executeQuery()) {

			Map<String, Map<String, String>>result = new HashMap<>();
			while(rs.next()) {
				Map<String, String> pair = new HashMap<>();
				pair.put("from", rs.getString("from"));
				pair.put("to", rs.getString("to"));
				result.put(rs.getString("pair"), pair);
			}

			return result;
		}catch(Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				this.close();
			}catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}

	/**
	 * 特定のfromに対するtoを取得
	 * @param from
	 * @return
	 * @throws DAOException
	 */
	public List<String> getgetPair(String from) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT * FROM pair WHERE from = ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, from);
			rs = st.executeQuery();

			List<String> result = new ArrayList<>();
			while(rs.next()) {
				result.add(rs.getString("to"));
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(st != null) {
					st.close();
				}
				this.close();
			}catch(Exception e) {
				throw new DAOException("リソースの開放に失敗しました");
			}
		}

	}
}
