package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Rate;

public class RateDAO extends BaseDAO{

	/**
	 * コンストラクタ
	 * @throws DAOException
	 */
	public RateDAO() throws DAOException {
		super();
	}

	/**
	 * 指定したfrom/toのログをlimitで指定した件数だけ取得
	 * @param from
	 * @param to
	 * @param limit
	 * @return
	 * @throws DAOException
	 */
	public List<Rate> getLog(String from, String to, int limit) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT * FROM log WHERE from = ? snd to = ? ORDER BY timestamp ASC LIMIT ? OFFSET (SELECT count(*) FROM log ) - ?";
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, from);
			st.setString(2, to);
			st.setInt(3, limit);
			rs = st.executeQuery();

			List<Rate> result = new ArrayList<>();
			while(rs.next()) {
				Rate rate = new Rate(new Date(rs.getTime("timestammp").getTime()), rs.getString("from"), rs.getString("to"), rs.getDouble("high"), rs.getDouble("open"), rs.getDouble("bid"), rs.getDouble("ask"), rs.getDouble("low"));
				result.add(rate);
			}

			return result;
		}catch(Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(st != null) {
					st.close();
				}
				this.close();
			}catch(Exception e) {
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}

	/**
	 * APIから取得した新しい為替レート情報をDBに追加する
	 * @param list
	 * @throws DAOException
	 */
	public void insert(List<Rate> list) throws DAOException {
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "INSERT INTO log VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement st = null;
		try {
			for(Rate rate : list) {
				st = this.getCon().prepareStatement(SQL);
				Timestamp timestamp = new Timestamp(rate.getTimestamp().getTime());
				st.setTimestamp(1, timestamp);
				st.setString(2, rate.getFrom());
				st.setString(3, rate.getTo());
				st.setDouble(4, rate.getHigh());
				st.setDouble(5, rate.getOpen());
				st.setDouble(6, rate.getBid());
				st.setDouble(7, rate.getAsk());
				st.setDouble(8, rate.getLow());
				st.executeUpdate();
			}
		}catch(Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの追記に失敗しました");
		}finally {
			try {
				if(st != null) {
					st.close();
				}
				this.close();
			}catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}


}
