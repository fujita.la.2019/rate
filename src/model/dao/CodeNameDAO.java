package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class CodeNameDAO extends BaseDAO{

	/**
	 * コンストラクタ
	 * @throws DAOException
	 */
	public CodeNameDAO() throws DAOException {
		super();
	}

	/**
	 * コードに対応した表示名をすべて取得
	 * @return 結果のMap
	 * @throws DAOException
	 */
	public Map<String, String> getAllCodeName() throws DAOException {
		//未接続れあれば接続
		if(this.getCon() == null) {
			this.getConection();
		}

		final String SQL = "SELECT * FROM code";
		try(PreparedStatement st = this.getCon().prepareStatement(SQL); ResultSet rs = st.executeQuery()) {

			Map<String, String>result = new HashMap<>();
			while(rs.next()) {
				//データの取得
				String key = rs.getString("code");
				String value = rs.getString("name");
				result.put(key, value);
			}

			return result;
		}catch(Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				this.close();
			}catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}

	public String getCodeName(String code) throws DAOException {
		//未接続れあれば接続
		if(this.getCon() == null) {
			this.getConection();
		}

		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			//SQL実行
			final String SQL = "SELECT * FROM code WHERE code = ?";
			st = this.getCon().prepareStatement(SQL);
			st.setString(1, code);
			rs = st.executeQuery();

			rs.next();
			return rs.getString("name");

		}catch(Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました");
		}finally {
			try {
				if(st != null) {
					st.close();
				}
				if(rs != null) {
					rs.close();
				}
				this.close();
			}catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました");
			}
		}
	}
}
