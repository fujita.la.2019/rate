package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.RateConnection;
import model.Rate;

/**
 * Servlet implementation class Home
 */
@WebServlet("/home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Home() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//パラメータ取得
		String code = request.getParameter("code");
		String money = request.getParameter("money");

		//レート情報取得
		RateConnection exchanger = new RateConnection();
		Rate rate = exchanger.getRateTable().get(code);
		request.setAttribute("rates", exchanger.getRateTable());


		//パラメータがセットされていれば値を格納
		if(code != null && money != null) {
			request.setAttribute("pairCode", code);
			request.setAttribute("money", money);
			request.setAttribute("result", rate);
			request.setAttribute("money", Integer.parseInt(money));
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/home.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
